describe("my test", function () {
    it ('Navigate', function () {
        cy.visit('http://localhost:3000')
        cy.contains("Войти").click()
    })

    // sign_in 
    it ('sign in', function () {
        cy.get(':nth-child(1) > .input_style_default > .input_control').type('776514377')
        cy.get('#password').type('Majsalim88')
        cy.get('.button').click()
        cy.wait(500)
    })

    // my mailings
    it ('my mailings', function () {
        cy.contains("Мои рассылки").click()
        cy.wait(500)
        cy.contains("Новая рассылка").click()
    })

    //create mailings 1-5 steps
    it ('create mailings 1-5 steps', function () {
        
        // mailing
        cy.get('#template_name').type("stage mailing test")
        
        // gender (all)
        cy.get(':nth-child(1) > .tab_radio__link').click()
        // cy.get(':nth-child(1s) > .tab_radio__link').click()

        // age (any)
        cy.get(':nth-child(2) > .dropdown > .dropdown__control > .selector').select('16-25').should('have.value', '0')
        
        // language 
        cy.contains('span', 'Язык обслуживания абонента')
        .parent('div')
        .find('input', 'checkbox')
        // .contains('span', 'Русский язык')
        .check('value', '0')
        // .check({ force: true })
        cy.get(':nth-child(2) > .application_form__section_content > .application_form__section_left > :nth-child(1) > .checkbox__label').click()
        cy.get(':nth-child(2) > .application_form__section_content > .application_form__section_left > :nth-child(2)').click()

        // interests
        cy.get(':nth-child(3) > .application_form__section_content > .application_form__section_left > :nth-child(1) > .checkbox__label').click()
        cy.get(':nth-child(3) > .application_form__section_content > .application_form__section_left > :nth-child(2) > .checkbox__label').click()
        cy.get(':nth-child(3) > .application_form__section_content > .application_form__section_left > :nth-child(3) > .checkbox__label').click()

        // cy.get('[data-cy=location_map]').should('have.text', 'location_map')
        cy.get('[style="z-index: 3; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; touch-action: pan-x pan-y;"]').click()
        cy.wait(500)

        // continue
        cy.contains('Продолжить').click()
        cy.wait(20000)
        })

        
        it ('auditory 2-5 step', function() {
        // SMS text

        cy.get("#textarea").type('my sms txt')
        cy.get('#tab_nav > :nth-child(2)').click()
        cy.get('[style="display: block;"] > .text_area > .text_area__content > #textarea').type('kyrgyz sms txt')
        // cy.get("#textarea").type('kyrgyz sms txt')
        cy.get(".button").click()
        })
        
        it ('auditory 3-4', function (){
        // Number of sms step 3
        cy.get('.button').click()
        })

        it ('auditory 4-5 step', function() {
        //date
        // cy.get('.react-calendar__tile--active > abbr').click()
        cy.wait(20000)
        cy.contains("Диапазон времени").click()
        cy.wait(20000)
        cy.get('.button').click()
        
        // cy.get('.button').click()

        // start mailing
        cy.get('.button').click()

      })

      it ('succesfull created', function(){
        //should have send
        cy.get('.success_info__title').should('have.class', 'success_info__title')
        cy.get('.button').click()
      })                                                                                        
      })